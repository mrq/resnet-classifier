# Tentative Title For A ResNet-Based Image Classifier

This is a simple ResNet based image classifier for images, using a similar training framework I use to train [VALL-E](https://git.ecker.tech/mrq/vall-e/).

## Premise

This was cobbled together in a night, partly to test how well my training framework fares when not married to my VALL-E implementation, and partly to solve a minor problem I faced.

This is by no ways state of the art, as it just leverages an existing ResNet arch provided by `torchvision`.

## Training

1. Throw the images you want to train under `./data/images/`.

2. Modify the `./data/config.yaml` accordingly.

3. Install using `pip3 install -e ./image_classifier/`.

4. Train using `python3 -m image_classifier.train --yaml='./data/config.yaml'`.

5. Wait.

## Inferencing

Simply invoke the inferencer with the following command: `python3 -m image_classifier --path="./data/path-to-your-image.png" --yaml="./data/config.yaml"`

### Continuous Usage

If you're looking to continuously classify images, use `python3 -m image_classifier --listen --port=7860 --yaml="./data/config.yaml"` instead to enable a light webserver using `simple_http_server`. Send a `GET` request to `http://127.0.0.1:7860/?b64={base64 encoded image string}` and a JSON response will be returned with the classified label.